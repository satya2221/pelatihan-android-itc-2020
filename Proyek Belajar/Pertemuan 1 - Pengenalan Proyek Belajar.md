# Pengenalan Proyek Belajar

- Untuk memudahkan dalam pembuatan proyek akhir, disini kita akan membuat proyek belajar.
- Proyek Belajar merupakan implementasi yang berkelanjutan pada setiap materi pertemuan yang ada, proyek belajar ini akan menjadi contoh sebagai membuat proyek akhir.
- Pada proyek belajar tidak akan dijelaskan secara detail penggunaan-penggunaan fungsi yang dipakai, jika ingin bertanya bisa berdiskusi pada forum saja.
- Untuk proyek akhir tetap harus membuat proyek sendiri, tidak boleh menggunakan proyek belajar ini, proyek belajar hanya contoh.