# Intent Pada Proyek Belajar

Mungkin dari kemarin kita telah membuat proyek dengan satu halaman activity saja, kali ini kita akan membuat proyek sebelumnya menjadi beberapa activity. Pada proyek ini kita akan membuat tiga halaman yaitu Login, Home, Profile. Dimana Login telah kita buat sebelumnya, sekarang kita akan menambahkan activity Home dan Profile, serta kita akan menghubungkan setiap activity tersebut.

Proyek ini kita akan melanjutkan proyek sebelumnya, jadi jika belum ada proyek sebelumnya silakan download hasil implementasi Proyek Belajar sebelumnya [disini](https://gitlab.com/RTAgung/poyek-belajar/-/tree/Pertemuan-3).

Mari kita mulai implementasinya, ikuti langkah-langkah berikut:
1. Buka Proyek Belajar yang sebelumnya
2. Buatlah dua activity baru dengan nama **HomeActivity** dan **ProfileActivity**, caranya bisa dengan klik kanan pada folder App **-->** New **-->** Activity **-->** Empty Activity **-->** isi nama activity yang sesuai **-->** Finish.
3. HomeActivity akan kita buat untuk menampilkan list movie, dan ProfileActivity akan kita buat untuk informasi profil kita.
4. Kita akan mulai terlebih dahulu pada HomeActivity, silakan buka `activity_home.xml` yang ada di dalam folder res **-->** layout. Lalu ubah code nya menjadi seperti berikut.
    ```xml
    <?xml version="1.0" encoding="utf-8"?>
    <ScrollView xmlns:android="http://schemas.android.com/apk/res/android"
        xmlns:app="http://schemas.android.com/apk/res-auto"
        xmlns:tools="http://schemas.android.com/tools"
        android:layout_width="match_parent"
        android:layout_height="match_parent"
        tools:context=".HomeActivity">
    
        <LinearLayout
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:orientation="vertical">
    
            <TextView
                android:id="@+id/tv_welcome"
                android:layout_width="match_parent"
                android:layout_height="wrap_content"
                android:gravity="center_horizontal"
                android:text="Welcome"
                android:textSize="24sp"
                android:textStyle="bold" />
    
            <RelativeLayout
                android:layout_width="match_parent"
                android:layout_height="wrap_content"
                android:layout_margin="16dp"
                android:background="#BBDEFB">
    
                <ImageView
                    android:id="@+id/iv_item_picture1"
                    android:layout_width="match_parent"
                    android:layout_height="wrap_content"
                    android:adjustViewBounds="true"
                    android:src="@drawable/movie" />
    
                <TextView
                    android:layout_width="match_parent"
                    android:layout_height="wrap_content"
                    android:layout_below="@id/iv_item_picture1"
                    android:gravity="center_horizontal"
                    android:text="Spongebob 1"
                    android:textColor="@color/black"
                    android:textSize="18sp"
                    android:textStyle="bold" />
            </RelativeLayout>
    
            <RelativeLayout
                android:layout_width="match_parent"
                android:layout_height="wrap_content"
                android:layout_margin="16dp"
                android:background="#BBDEFB">
    
                <ImageView
                    android:id="@+id/iv_item_picture2"
                    android:layout_width="match_parent"
                    android:layout_height="wrap_content"
                    android:adjustViewBounds="true"
                    android:src="@drawable/movie" />
    
                <TextView
                    android:layout_width="match_parent"
                    android:layout_height="wrap_content"
                    android:layout_below="@id/iv_item_picture2"
                    android:gravity="center_horizontal"
                    android:text="Spongebob 2"
                    android:textColor="@color/black"
                    android:textSize="18sp"
                    android:textStyle="bold" />
            </RelativeLayout>
    
            <RelativeLayout
                android:layout_width="match_parent"
                android:layout_height="wrap_content"
                android:layout_margin="16dp"
                android:background="#BBDEFB">
    
                <ImageView
                    android:id="@+id/iv_item_picture3"
                    android:layout_width="match_parent"
                    android:layout_height="wrap_content"
                    android:adjustViewBounds="true"
                    android:src="@drawable/movie" />
    
                <TextView
                    android:layout_width="match_parent"
                    android:layout_height="wrap_content"
                    android:layout_below="@id/iv_item_picture3"
                    android:gravity="center_horizontal"
                    android:text="Spongebob 3"
                    android:textColor="@color/black"
                    android:textSize="18sp"
                    android:textStyle="bold" />
            </RelativeLayout>
        </LinearLayout>
    </ScrollView>
    ```
    disini kita menggunakan ScrollView agar dapat di-scroll jika datanya banyak
5. Kemudian kita buat juga tampilan untuk ProfileActivity, silakan buka `activity_profile.xml` dan ubah code nya seperti ini.
    ```xml
    <?xml version="1.0" encoding="utf-8"?>
    <LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
        xmlns:app="http://schemas.android.com/apk/res-auto"
        xmlns:tools="http://schemas.android.com/tools"
        android:layout_width="match_parent"
        android:layout_height="match_parent"
        android:gravity="center"
        android:orientation="vertical"
        android:padding="16dp"
        tools:context=".ProfileActivity">
    
        <ImageView
            android:layout_width="200dp"
            android:layout_height="200dp"
            android:layout_margin="16dp"
            android:src="@drawable/profile" />
    
        <TextView
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:gravity="center_horizontal"
            android:text="Rama Tri Agung"
            android:textSize="20sp" />
    
        <TextView
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:gravity="center_horizontal"
            android:text="ramatriagung91@gmail.com"
            android:textSize="16sp" />
    
        <Button
            android:id="@+id/btn_instagram"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:layout_margin="16dp"
            android:text="My Instagram" />
    
    </LinearLayout>
    ```
    Profile hanya menampilkan gambar, nama, dan link Instagram.
6. Setelah semua View telah dibuat sekarang kita akan memperbaiki fungsi login yang ada di `MainActivity.java` agar dapat berpindah halaman ke HomeActivity dengan cara berikut.
    ```java
    public class MainActivity extends AppCompatActivity implements View.OnClickListener {
        
        // inisialisasi key pada data yang akan dikirim lewat intent
        public static final String EXTRA_USERNAME = "USERNAME";
    
        ...
        
        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_main);
            // mengganti tulisan di action bar (paling atas)
            getSupportActionBar().setTitle("Login");
    
            ...
            
        }
        
        ...
        
        private void login() {
            // mengambil nilai string pada text input di view
            String username = inputUsername.getText().toString();
            String password = inputPassword.getText().toString();
    
            // memeriksa apakah inputnya kosong atau tidak
            // jika kosong diberikan error "username couldn't be empty"
            if (username.isEmpty()) inputUsername.setError("username couldn't be empty");
            else if (password.isEmpty()) inputPassword.setError("password couldn't be empty");
            else {
                // melakukan pemeriksaan login
                if (password.equals("admin123")) {
                    // deklarasi intent untuk pindah halaman ke HomeActivity
                    Intent intent = new Intent(this, HomeActivity.class);
                    // memasukkan data yang akan dikirimkan dengan nama kunci EXTRA_USERNAME
                    intent.putExtra(EXTRA_USERNAME, username);
                    // memulai pindah halaman
                    startActivity(intent);
                    // mengakhiri halaman agar tidak dapat kembali ke halaman ini (MainActivity)
                    finish();
    
                    Toast.makeText(this, "login success", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(this, "login failed", Toast.LENGTH_SHORT).show();
                }
            }
        }
    }
    ```
    Disini kita akan membuat username yang diinput akan dikirimkan kedalam HomeActivity, dan login berhasil hanya jika password sama dengan admin123.
    Silakan dicoba terlebih dahulu jika ingin melihat proses yang terjadi.
7. Selanjutnya kita akan menambahkan code di `HomeActivity.java`.
    ```java
    public class HomeActivity extends AppCompatActivity {
    
        // inisialisasi key pada data yang akan dikirim lewat intent
        private TextView welcome;
    
        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_home);
            // mengganti tulisan di action bar (paling atas)
            getSupportActionBar().setTitle("Home");
    
            // koneksikan variabel dengan view berdasarkan id
            welcome = findViewById(R.id.tv_welcome);
    
            // menerima kiriman data dari halaman sebelumnya menggunakan kuncinya (EXTRA_USERNAME)
            String username = getIntent().getStringExtra(MainActivity.EXTRA_USERNAME);
    
            // menampilkan text ke dalam view
            welcome.setText("Welcome, " + username);
        }
    ```
8. Kemudian bagaimana caranya kita dapat berpindah activity dari HomeActivity ke ProfileActivity. Disini kita akan membuat tombol menu di bagian atas pojok kanan. Caranya kita harus membuat terlebih dahulu tampilan menu tersebut di folder res.
9. Buatlah folder menu di dalam folder res, caranya dengan klik kanan folder res **-->** New **-->** Android Resource Directory **-->** ganti Resource Type menjadi menu **-->** klik OK.
10. Setelah itu kita buat file `menu_home.xml` di dalam folder menu, caranya dengan klik kanan folder menu **-->** New **-->** Menu Resource File **-->** isi file name dengan **menu_home** **-->** klik OK.
11. Kemudian buka `menu_home.xml` isi codenya menjadi seperti berikut.
    ```xml
    <?xml version="1.0" encoding="utf-8"?>
    <menu xmlns:android="http://schemas.android.com/apk/res/android"
        xmlns:app="http://schemas.android.com/apk/res-auto">
        <item
            android:id="@+id/action_profile"
            android:icon="@drawable/ic_baseline_person_24"
            android:title="Profile"
            app:showAsAction="always" />
    </menu>
    ```
12. Kita sudah berhasil membuat tampilan menu yang ada di atas pojok kanan, selanjutnya kita akan masukkan tampilan menu tersebut ke dalam HomeActivity, caranya dengan tambahkan code berikut di dalam `HomeActivity.java`.
    ```java
    public class HomeActivity extends AppCompatActivity {
    
        ...
    
        // memunculkan tombol yang ada di action bar
        @Override
        public boolean onCreateOptionsMenu(Menu menu) {
            // menghubungkan tombol-tombol dengan menu yang telah dibuat sebelumnya
            getMenuInflater().inflate(R.menu.menu_home, menu);
            return super.onCreateOptionsMenu(menu);
        }
    }
    ```
13. Kemudian kita tambahkan fungsi aksi ketika tombol menu tersebut ditekan, tambahkan code berikut di `HomeActivity.java`.
    ```java
    public class HomeActivity extends AppCompatActivity {
    
        ...
    
        // membuat aksi pada tombol yang ada di action bar (paling atas)
        @Override
        public boolean onOptionsItemSelected(@NonNull MenuItem item) {
            // identifikasi tombol mana yang diklik berdasarkan id
            switch (item.getItemId()) {
                // tombol profile
                case R.id.action_profile:
                    // deklarasi intent seperti biasa
                    Intent intent = new Intent(this, ProfileActivity.class);
                    // memulai pindah halaman
                    startActivity(intent);
                    break;
            }
            return super.onOptionsItemSelected(item);
        }
    }
    ```
14. Dengan begitu kita telah berhasil pindah halaman dari HomeActivity ke ProfileActivity. Langkah terakhir kita akan menyempurnakan ProfileActivity.
15. Buka file `ProfileActivity.java`, ubah code nya menjadi seperti ini.
    ```java
    public class ProfileActivity extends AppCompatActivity {
    
        // inisialisasi key pada data yang akan dikirim lewat intent
        private Button btnInstagram;
    
        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_profile);
            // mengganti tulisan di action bar (paling atas)
            getSupportActionBar().setTitle("Profile");
    
            // memunculkan tombol back di action bar
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    
            // koneksikan variabel dengan view berdasarkan id
            btnInstagram = findViewById(R.id.btn_instagram);
    
            // inisialisasi fungsi klik pada tombol
            btnInstagram.setOnClickListener(v -> {
                // deklarasi intent untuk pindah halaman ke link yang disertakan
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.instagram.com/_rtagung/"));
                // memulai pindah halaman
                startActivity(intent);
            });
        }
    
        // membuat aksi pada tombol yang ada di action bar (paling atas)
        @Override
        public boolean onOptionsItemSelected(@NonNull MenuItem item) {
            // identifikasi tombol mana yang diklik berdasarkan id
            switch (item.getItemId()) {
                // tombol back pada action bar
                case android.R.id.home:
                    finish();
                    break;
            }
            return super.onOptionsItemSelected(item);
        }
    }
    ```
16. Sehingga isi dari ketiga file tersebut adalah
    - `MainActivity.java`
        ```java
        public class MainActivity extends AppCompatActivity implements View.OnClickListener {
        
            // inisialisasi key pada data yang akan dikirim lewat intent
            public static final String EXTRA_USERNAME = "USERNAME";
        
            // deklarasi variabel view sesuai dengan layout xml
            private TextInputEditText inputUsername, inputPassword;
            private Button btnLogin, btnSignup;
        
            @Override
            protected void onCreate(Bundle savedInstanceState) {
                super.onCreate(savedInstanceState);
                setContentView(R.layout.activity_main);
                // mengganti tulisan di action bar (paling atas)
                getSupportActionBar().setTitle("Login");
        
                // koneksikan variabel dengan view berdasarkan id
                inputUsername = findViewById(R.id.input_username);
                inputPassword = findViewById(R.id.input_password);
                btnLogin = findViewById(R.id.btn_login);
                btnSignup = findViewById(R.id.btn_signup);
        
                // inisialisasi fungsi klik pada tombol
                btnLogin.setOnClickListener(this);
                btnSignup.setOnClickListener(this);
            }
        
            @SuppressLint("NonConstantResourceId")
            @Override
            public void onClick(View v) {
                // identifikasi tombol mana yang diklik berdasarkan id
                switch (v.getId()) {
                    case R.id.btn_login:
                        login();
                        break;
                    case R.id.btn_signup:
                        signup();
                        break;
                }
            }
        
            private void signup() {
                // menampilkan notif
                Toast.makeText(this, "you have been registered", Toast.LENGTH_SHORT).show();
            }
        
            private void login() {
                // mengambil nilai string pada text input di view
                String username = inputUsername.getText().toString();
                String password = inputPassword.getText().toString();
        
                // memeriksa apakah inputnya kosong atau tidak
                // jika kosong diberikan error "username couldn't be empty"
                if (username.isEmpty()) inputUsername.setError("username couldn't be empty");
                else if (password.isEmpty()) inputPassword.setError("password couldn't be empty");
                else {
                    // melakukan pemeriksaan login
                    if (password.equals("admin123")) {
                        // deklarasi intent untuk pindah halaman ke HomeActivity
                        Intent intent = new Intent(this, HomeActivity.class);
                        // memasukkan data yang akan dikirimkan dengan nama kunci EXTRA_USERNAME
                        intent.putExtra(EXTRA_USERNAME, username);
                        // memulai pindah halaman
                        startActivity(intent);
                        // mengakhiri halaman agar tidak dapat kembali ke halaman ini (MainActivity)
                        finish();
        
                        Toast.makeText(this, "login success", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(this, "login failed", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        }
        ```
    - `HomeActivity.java`
        ```java
        public class HomeActivity extends AppCompatActivity {
        
            // inisialisasi key pada data yang akan dikirim lewat intent
            private TextView welcome;
        
            @Override
            protected void onCreate(Bundle savedInstanceState) {
                super.onCreate(savedInstanceState);
                setContentView(R.layout.activity_home);
                // mengganti tulisan di action bar (paling atas)
                getSupportActionBar().setTitle("Home");
        
                // koneksikan variabel dengan view berdasarkan id
                welcome = findViewById(R.id.tv_welcome);
        
                // menerima kiriman data dari halaman sebelumnya menggunakan kuncinya (EXTRA_USERNAME)
                String username = getIntent().getStringExtra(MainActivity.EXTRA_USERNAME);
        
                // menampilkan text ke dalam view
                welcome.setText("Welcome, " + username);
            }
        
            // memunculkan tombol yang ada di action bar
            @Override
            public boolean onCreateOptionsMenu(Menu menu) {
                // menghubungkan tombol-tombol dengan menu yang telah dibuat sebelumnya
                getMenuInflater().inflate(R.menu.menu_home, menu);
                return super.onCreateOptionsMenu(menu);
            }
        
            // membuat aksi pada tombol yang ada di action bar (paling atas)
            @Override
            public boolean onOptionsItemSelected(@NonNull MenuItem item) {
                // identifikasi tombol mana yang diklik berdasarkan id
                switch (item.getItemId()) {
                    // tombol profile
                    case R.id.action_profile:
                        // deklarasi intent seperti biasa
                        Intent intent = new Intent(this, ProfileActivity.class);
                        // memulai pindah halaman
                        startActivity(intent);
                        break;
                }
                return super.onOptionsItemSelected(item);
            }
        }
        ```
    - `ProfileActivity.java`
        ```java
        public class ProfileActivity extends AppCompatActivity {
        
            // inisialisasi key pada data yang akan dikirim lewat intent
            private Button btnInstagram;
        
            @Override
            protected void onCreate(Bundle savedInstanceState) {
                super.onCreate(savedInstanceState);
                setContentView(R.layout.activity_profile);
                // mengganti tulisan di action bar (paling atas)
                getSupportActionBar().setTitle("Profile");
        
                // memunculkan tombol back di action bar
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        
                // koneksikan variabel dengan view berdasarkan id
                btnInstagram = findViewById(R.id.btn_instagram);
        
                // inisialisasi fungsi klik pada tombol
                btnInstagram.setOnClickListener(v -> {
                    // deklarasi intent untuk pindah halaman ke link yang disertakan
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.instagram.com/_rtagung/"));
                    // memulai pindah halaman
                    startActivity(intent);
                });
            }
        
            // membuat aksi pada tombol yang ada di action bar (paling atas)
            @Override
            public boolean onOptionsItemSelected(@NonNull MenuItem item) {
                // identifikasi tombol mana yang diklik berdasarkan id
                switch (item.getItemId()) {
                    // tombol back pada action bar
                    case android.R.id.home:
                        finish();
                        break;
                }
                return super.onOptionsItemSelected(item);
            }
        }
        ```
    Silakan dicoba terlebih dahulu

Selamat! kita berhasil mengembangkan proyek belajar menjadi banyak halaman activity dan menghubungkannya.

source code dapat diakses pada link berikut :
[Intent Pada Proyek Belajar](https://gitlab.com/RTAgung/poyek-belajar/-/tree/Pertemuan-4)
