# Recycler View Lanjutan Pada Proyek Belajar

Sebelumnya kita hanya membuat list movie, namun bagaimana jika tiap item list tersebut bisa di klik dan berpindah ke halaman detail. Pada implementasi kali ini, kita akan membuat list item Recycler View memiliki aksi untuk pindah ke halaman Detail pada setiap item. Untuk menerapkannya kita hanya mengubah beberapa code yang ada pada Adapter saja.

Proyek ini kita akan melanjutkan proyek sebelumnya, jadi jika belum ada proyek sebelumnya silakan download hasil implementasi Proyek Belajar sebelumnya [disini](https://gitlab.com/RTAgung/poyek-belajar/-/tree/Pertemuan-5).

Mari kita mulai implementasinya, ikuti langkah-langkah berikut:
1. Buka Proyek Belajar yang sebelumnya.
2. Buatlah activity baru untuk tujuan pindah halaman detail dengan nama DetailActivity, agar DetailActivity berada pada package ui, kalian dapat memulai klik kanan pada package ui **-->** New **-->** Activity **-->** Empty Activity **-->** isi nama DetailActivity **-->** Finish.
3. Jika sudah, buka `activity_detail.xml` lalu ubah code nya menjadi seperti ini.
    ```xml
    <?xml version="1.0" encoding="utf-8"?>
    <RelativeLayout xmlns:android="http://schemas.android.com/apk/res/android"
        xmlns:app="http://schemas.android.com/apk/res-auto"
        xmlns:tools="http://schemas.android.com/tools"
        android:layout_width="match_parent"
        android:layout_height="match_parent"
        android:padding="16dp"
        tools:context=".ui.DetailActivity">
    
        <ImageView
            android:id="@+id/iv_detail_poster"
            android:layout_width="160dp"
            android:layout_height="wrap_content"
            android:layout_marginEnd="12dp"
            android:layout_marginBottom="16dp"
            android:adjustViewBounds="true"
            android:src="@drawable/poster" />
    
        <TextView
            android:id="@+id/tv_detail_title"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:layout_marginBottom="8dp"
            android:layout_toEndOf="@id/iv_detail_poster"
            android:text="Spongebob"
            android:textSize="20sp"
            android:textStyle="bold" />
    
        <TextView
            android:id="@+id/tv_detail_original_title"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:layout_below="@id/tv_detail_title"
            android:layout_marginBottom="8dp"
            android:layout_toEndOf="@id/iv_detail_poster"
            android:text="Spongebob"
            android:textSize="16sp"
            android:textStyle="italic" />
    
        <TextView
            android:id="@+id/tv_detail_release"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:layout_alignBottom="@id/iv_detail_poster"
            android:layout_toEndOf="@id/iv_detail_poster"
            android:text="Release on 2020-08-14"
            android:textSize="14sp" />
    
        <TextView
            android:id="@+id/tv_detail_overview"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:layout_below="@id/iv_detail_poster"
            android:layout_marginBottom="12dp"
            android:text="Overview"
            android:textSize="16sp"
            android:textStyle="bold" />
    
        <TextView
            android:id="@+id/tv_detail_overview_text"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:layout_below="@id/tv_detail_overview"
            android:text="When his best friend Gary is suddenly snatched away, SpongeBob takes Patrick on a madcap mission far beyond Bikini Bottom to save their pink-shelled pal."
            android:textSize="16sp" />
    
    </RelativeLayout>
    ```
4. Selanjutnya kita akan modifikasi beberapa kelas yaitu model `Movie.java` agar data dapat ditampung lebih banyak, lalu penyedia data `DataDummy.java` agar data movie lebih detail, kemudian `DetailActivity.java` dan terakhir `MovieAdapter.java`.
5. Mari kita mulai terlebih dahulu pada kelas Movie.java. Tambahkan beberapa code sehingga menjadi seperti ini.
    ```java
    // membuat kelas model untuk menampung data data movie
    public class Movie {
        private String id;
        private String title;
        private String originalTitle;
        private String overview;
        private String releaseDate;
        private String backdropPath;
        private String posterPath;
    
        public Movie(String id, String title, String originalTitle, String overview, String releaseDate, String backdropPath, String posterPath) {
            this.id = id;
            this.title = title;
            this.originalTitle = originalTitle;
            this.overview = overview;
            this.releaseDate = releaseDate;
            this.backdropPath = backdropPath;
            this.posterPath = posterPath;
        }
    
        public String getId() {
            return id;
        }
    
        public String getTitle() {
            return title;
        }
    
        public String getOriginalTitle() {
            return originalTitle;
        }
    
        public String getOverview() {
            return overview;
        }
    
        public String getReleaseDate() {
            return releaseDate;
        }
    
        public String getBackdropPath() {
            return backdropPath;
        }
    
        public String getPosterPath() {
            return posterPath;
        }
    }
    ```
6. Selanjutnya kita tambahkan juga data yang ada di `DataDummy.java` menjadi seperti berikut.
    ```java
    public class DataDummy {
        // mengisi list data sementara
        public static ArrayList<Movie> getDummyMovie() {
            ArrayList<Movie> listMovie = new ArrayList<>();
    
            listMovie.add(
                    new Movie(
                            "590706",
                            "Jiu Jitsu",
                            "Jiu Jitsu",
                            "Every six years, an ancient order of jiu-jitsu fighters joins forces to battle a vicious race of alien invaders. But when a celebrated war hero goes down in defeat, the fate of the planet and mankind hangs in the balance.",
                            "2020-11-20",
                            "/jeAQdDX9nguP6YOX6QSWKDPkbBo.jpg",
                            "/eLT8Cu357VOwBVTitkmlDEg32Fs.jpg"
                    )
            );
            listMovie.add(
                    new Movie(
                            "602211",
                            "Fatman",
                            "Fatman",
                            "A rowdy, unorthodox Santa Claus is fighting to save his declining business. Meanwhile, Billy, a neglected and precocious 12 year old, hires a hit man to kill Santa after receiving a lump of coal in his stocking.",
                            "2020-11-13",
                            "/ckfwfLkl0CkafTasoRw5FILhZAS.jpg",
                            "/4n8QNNdk4BOX9Dslfbz5Dy6j1HK.jpg"
                    )
            );
            listMovie.add(
                    new Movie(
                            "671583",
                            "Upside-Down Magic",
                            "Upside-Down Magic",
                            "Nory and her best friend Reina enter the Sage Academy for Magical Studies, where Nory’s unconventional powers land her in a class for those with wonky, or “upside-down,” magic. Undaunted, Nory sets out to prove that that upside-down magic can be just as powerful as right-side-up.",
                            "2020-07-31",
                            "/vam9VHLZl8tqNwkp1zjEAxIOrkk.jpg",
                            "/h9vTJ78h0SASYUxg4jV0AQ00oF2.jpg"
                    )
            );
            listMovie.add(
                    new Movie(
                            "590706",
                            "Jiu Jitsu",
                            "Jiu Jitsu",
                            "Every six years, an ancient order of jiu-jitsu fighters joins forces to battle a vicious race of alien invaders. But when a celebrated war hero goes down in defeat, the fate of the planet and mankind hangs in the balance.",
                            "2020-11-20",
                            "/jeAQdDX9nguP6YOX6QSWKDPkbBo.jpg",
                            "/eLT8Cu357VOwBVTitkmlDEg32Fs.jpg"
                    )
            );
            listMovie.add(
                    new Movie(
                            "602211",
                            "Fatman",
                            "Fatman",
                            "A rowdy, unorthodox Santa Claus is fighting to save his declining business. Meanwhile, Billy, a neglected and precocious 12 year old, hires a hit man to kill Santa after receiving a lump of coal in his stocking.",
                            "2020-11-13",
                            "/ckfwfLkl0CkafTasoRw5FILhZAS.jpg",
                            "/4n8QNNdk4BOX9Dslfbz5Dy6j1HK.jpg"
                    )
            );
            listMovie.add(
                    new Movie(
                            "671583",
                            "Upside-Down Magic",
                            "Upside-Down Magic",
                            "Nory and her best friend Reina enter the Sage Academy for Magical Studies, where Nory’s unconventional powers land her in a class for those with wonky, or “upside-down,” magic. Undaunted, Nory sets out to prove that that upside-down magic can be just as powerful as right-side-up.",
                            "2020-07-31",
                            "/vam9VHLZl8tqNwkp1zjEAxIOrkk.jpg",
                            "/h9vTJ78h0SASYUxg4jV0AQ00oF2.jpg"
                    )
            );
    
            return listMovie;
        }
    
        // mengambil detail data dari salah satu item list data
        public static Movie getDummyDetailMovie(String id) {
            ArrayList<Movie> listMovie = getDummyMovie();
            Movie movie = null;
    
            for (int i = 0; i < listMovie.size(); i++) {
                if (listMovie.get(i).getId().equals(id)) {
                    movie = listMovie.get(i);
                }
            }
    
            return movie;
        }
    }
    ```
7. Jika data sudah disediakan dan siap untuk ditampilkan, selanjutnya kita akan menambahkan code pada `DetailActivity.java` untuk mengatur view nya.
    ```java
    public class DetailActivity extends AppCompatActivity {
    
        // deklarasi kunci / key untuk pengiriman data id
        public static final String EXTRA_ID = "extra_id";
    
        // membuat variabel konstanta untuk melengkapi link yang kurang
        private static final String IMAGE_URL = "https://image.tmdb.org/t/p/w500/";
    
        // deklarasi variabel view sesuai dengan layout xml
        private TextView tvTitle, tvOriginalTitle, tvReleaseDate, tvOverview;
        private ImageView ivPoster;
    
        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_detail);
            // mengganti tulisan di action bar (paling atas)
            getSupportActionBar().setTitle("Detail");
            // memunculkan tombol back di action bar
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    
            // menerima id yang dikirim dari HomeActivity
            String id = getIntent().getStringExtra(EXTRA_ID);
    
            // koneksikan variabel dengan view berdasarkan id
            tvTitle = findViewById(R.id.tv_detail_title);
            tvOriginalTitle = findViewById(R.id.tv_detail_original_title);
            tvReleaseDate = findViewById(R.id.tv_detail_release);
            tvOverview = findViewById(R.id.tv_detail_overview_text);
            ivPoster = findViewById(R.id.iv_detail_poster);
    
            // mengambil data dari data sementara (data dummy) berdasarkan id
            Movie movie = DataDummy.getDummyDetailMovie(id);
    
            // memanggil method untuk menampilkan data ke dalam view (tampilan)
            populateView(movie);
        }
    
        // membuat aksi pada tombol yang ada di action bar (paling atas)
        @Override
        public boolean onOptionsItemSelected(@NonNull MenuItem item) {
            // identifikasi tombol mana yang diklik berdasarkan id
            switch (item.getItemId()) {
                // tombol back pada action bar
                case android.R.id.home:
                    finish();
                    break;
            }
            return super.onOptionsItemSelected(item);
        }
    
        // untuk menampilkan data ke dalam view (tampilan)
        private void populateView(Movie movie) {
            // menampilkan text ke dalam view item
            tvTitle.setText(movie.getTitle());
            tvOriginalTitle.setText(movie.getOriginalTitle());
            tvReleaseDate.setText("Release on " + movie.getReleaseDate());
            tvOverview.setText(movie.getOverview());
    
            // mengambil dan menampilkan image berdasarkan link ke dalam view item
            Glide.with(this)
                    .load(IMAGE_URL + movie.getPosterPath())
                    .into(ivPoster);
        }
    }
    ```
8. Langkah terakhir kita akan membuat aksi pada setiap item list yang ada di HomeActivity agar dapat berpindah ke dalam DetailActivity dengan mengirim data detail movie. Caranya dengan menambahkan code pada `MovieAdapter.java` di dalam void bind() seperti ini.
    ```java
    // membuat kelas adapter sebagai penghubung antara list dan item nya
    public class MovieAdapter extends RecyclerView.Adapter<MovieAdapter.ViewHolder> {
        ...
        
        // membuat kelas sebagai kontrol item di dalam view (movie_item.xml) agar dapat dimanipulasi
        public class ViewHolder extends RecyclerView.ViewHolder {
            ...
    
            public void bind(Movie movie) {
                ...
    
                // melakukan pindah halaman ke halaman detail
                itemView.setOnClickListener(v -> {
                    Intent intent = new Intent(itemView.getContext(), DetailActivity.class);
                    // mengirim data id ke detail dengan key tertentu (EXTRA_ID)
                    intent.putExtra(EXTRA_ID, movie.getId());
                    // memulai pindah halaman dengan bantuan context dari itemView
                    itemView.getContext().startActivity(intent);
                });
            }
        }
    }
    ```
9. Dengan menambah code diatas, maka code keseluruhan pada `MovieAdapter.java` akan seperti ini.
    ```java
    // membuat kelas adapter sebagai penghubung antara list dan item nya
    public class MovieAdapter extends RecyclerView.Adapter<MovieAdapter.ViewHolder> {
        // deklarasi list yang akan ditampilkan
        private ArrayList<Movie> movies = new ArrayList<>();
    
        // mengisi data list yang akan di tampilkan
        public void setMovies(ArrayList<Movie> movies) {
            if (movies != null) {
                this.movies.clear();
                this.movies.addAll(movies);
            }
            // update tampilan jika terdapat perubahan data
            notifyDataSetChanged();
        }
    
        @NonNull
        @Override
        public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            // mengkoneksikan layout item kedalam kelas adapter ini
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.movie_item, parent, false);
            return new ViewHolder(view);
        }
    
        @Override
        public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
            // mengirimkan data tiap satuan item ke kelas ViewHolder untuk ditampilkan
            holder.bind(movies.get(position));
        }
    
        // mengambil panjang list yang ada
        @Override
        public int getItemCount() {
            return movies.size();
        }
    
        // membuat kelas sebagai kontrol item di dalam view (movie_item.xml) agar dapat dimanipulasi
        public class ViewHolder extends RecyclerView.ViewHolder {
            // membuat variabel konstanta untuk melengkapi link yang kurang
            private static final String IMAGE_URL = "https://image.tmdb.org/t/p/w500/";
    
            // deklarasi variabel view sesuai dengan layout xml
            private TextView tvTitle;
            private ImageView ivImage;
    
            public ViewHolder(@NonNull View itemView) {
                super(itemView);
                // koneksikan variabel dengan view berdasarkan id
                tvTitle = itemView.findViewById(R.id.tv_item_title);
                ivImage = itemView.findViewById(R.id.iv_item_image);
            }
    
            public void bind(Movie movie) {
                // menampilkan text ke dalam view item
                tvTitle.setText(movie.getTitle());
                // mengambil dan menampilkan image berdasarkan link ke dalam view item
                Glide.with(itemView.getContext())
                        .load(IMAGE_URL + movie.getBackdropPath())
                        .into(ivImage);
            }
        }
    }
    ```
10. Jika sudah, silakan dijalankan aplikasi nya dan coba untuk menekan item movie pada list.

Selamat! kita sekarang sudah mengerti bagaimana cara membuat aksi klik pada item di Recycler View. Pada pertemuan selanjutnya kita akan membuat data yang akan digunakan bersifat dinamis. Tetap Semangaatt!!

source code dapat diakses pada link berikut :
[Recycler View Lanjutan Pada Proyek Belajar](https://gitlab.com/RTAgung/poyek-belajar/-/tree/Pertemuan-6)
