# API
### Pendahuluan
Pada pertemuan-pertemuan sebelumnya, kita sudah membahas tentang apa itu RecyclerView dan bagaimana cara memakainya. Disana kita membuat Dummy Data untuk ditampilkan ke dalam RecyclerView kita. Pada pertemuan kali ini, kita akan menggunakan data dalam bentuk yang sudah jadi dan siap diolah di dalam project kita. Untuk mengolah dan mengelola data tersebut, kita membutuhkan yang namanya API. 

Menurut Wikipedia Id "Application Programming Interface disingkat API adalah penerjemah komunikasi antara klien dengan server untuk menyederhanakan implementasi dan perbaikan software". Dari pengertian tersebut dapat disimpulkan bahwa API adalah sebagai tempat interaksi data antara server dengan platform seperti android, web, ios, dll. Data yang dikomunikasikan oleh API adalah data yang sudah berbentuk JSON maka kita perlu menerjemahkannya di dalam android studio menjadi Model Data yang pernah kita buat pada project RecyclerView.

### Intergrasi API
Agar anda dapat lebih memahami tentang materi API ini, kita masuk ke dalam project yang akan mencontohkan penggunaan API secara langsung. Namun implementasi pada API ini kita lakukan tidak dengan menampilkan data ke dalam View, tapi hanya menampilkan data pada Console Android Studio.
1. Buatlah project baru di Android Studio dengan nama CoronaAPI.

2. Setelah project dibuat, tambahkan beberapa dependensi yang akan kita gunakan untuk project kita kali ini. Dependensi akan ditambahkan dalam file `build.gradle (module: app)` lalu pada bagian dependencies seperti ini.
    ```gradle
    implementation 'com.squareup.retrofit2:retrofit:2.9.0'
    implementation 'com.squareup.retrofit2:converter-gson:2.9.0'
    ```
    Pada kodingan diatas kita menggunakan library Retrofit untuk memudahkan proses akses data API. Untuk memahami lebih lanjut tentang Retrofit bisa melihat tautan ini [Retrofit](https://square.github.io/retrofit/).
    
    Setelah itu tambahkan compileOption pada bagian android seperti ini,
    
    ```gradle
    android{
    ...
    
        compileOptions {
            sourceCompatibility JavaVersion.VERSION_1_8
            targetCompatibility JavaVersion.VERSION_1_8
        }
    }
    ```
    
3. Dikarenakan kita akan menggunakan data API yang berada di internet, kita perlu memberikan akses internet pada project kita agar data dapat terambil dengan lancar. Caranya adalah dengan menambahkan sebaris kode di dalam sebuah file `AndroidManifest.xml` di dalam package _**manifest**_ seperti berikut ini,
    ```java
    <manifest xmlns:android="http://schemas.android.com/apk/res/android"
        package="com.example.coronaapi">

        <uses-permission android:name="android.permission.INTERNET" />

        ...
    </manifest>
    ```
    
4. Selanjutnya kita akan membutuhkan sebuah _**Plugin**_ yang bernama _**RoboPOJOGenerator**_. _**RoboPOJOGenerator**_ adalah plugin yang membantu kita dalam menerjemah Model JSON dari API ke Model Data yang akan digunakan. Caranya adalah pilih _**File**_ -> _**Settings**_ -> _**Plugins**_ -> _**Marketplace**_ -> lalu cari _**RoboPOJOGenerator**_. Setelah itu, install dan restart IDE.

    ![RoboPOJO](https://gitlab.com/args06/pelatihan-android-itc-2020/-/raw/master/images/robopojo.png)

5. Setelah selesai menginstall _**RoboPOJOGenerator**_, mari kita mulai masuk ke dalam API-nya. Buatlah dua buah package di dalam package utama dengan nama _**model**_ dan _**service**_. Caranya adalah dengan melakukan klik kanan pada package utama pada project aplikasi Anda -> _**New**_ ->  _**Package**_ -> Lalu tuliskan nama packagenya.

6. Langkah selanjutnya, buka link [Corona API](https://api.covid19api.com/summary) dan kita copy seluruh isinya. Setelah dicopy, lalu kita klik kanan pada package _**model**_ -> _**Generate POJO from JSON**_ -> Setelah terbuka dialog _**RoboPOJO**_, kita paste apa yang kita copy. Setelah itu, ubah _**Root object name**_ menjadi _**CoronaResponse**_, pilih _**Language**_ menjadi _**Java**_, _**Framework**_ menjadi _**GSON**_, checklist semua yang berada di bawahnya, dan klik _**Generate**_. Setelah di _**Generate**_, di dalam package _**model**_ akan ada 3 java class dengan nama _**CoronaResponse**_, _**CountriesItem**_, dan _**Global**_.

    ![RoboPOJO2](https://gitlab.com/args06/pelatihan-android-itc-2020/-/raw/master/images/robopojo2.png)
    
    > Apabila tampilan web pada link Corona Api berantakan dan sulit dibaca, kalian bisa instal extension browser yang bernama JsonFormatter agar data JSON tersebut mudah dibaca.

7. Setelah selesai dengan _**RoboPOJOGenerator**_, kita akan membuat 1 interface dengan nama _**CoronaAPI**_ dan 1 class dengan nama _**CoronaService**_ di dalam package _**service**_. Untuk membuat interface hampir sama dengan membuat class, perbedaannya adalah dengan memilih interface saat memasukkan namanya.

    ![Interface](https://gitlab.com/args06/pelatihan-android-itc-2020/-/raw/master/images/interface.png)

8. Setelah itu sesuaikan kodingan yang berada di interface dan kelas tersebut seperti berikut ini,
    - `CoronaAPI.java`
        ```java
        public interface CoronaAPI {
            String URL_BASE = "https://api.covid19api.com/";
            
            @GET("summary")
            Call<CoronaResponse> getCorona();
        }
        ```
        - `URL_BASE` digunakan untuk menyimpan base dari url yang kita gunakan tadi.
        - Dikarenakan kita hanya mengambil data dan menggunakannya di dalam project kita, kita menggunakan query `@GET` dengan isi sisa dari url yang kita gunakan tadi.
        - `CoronaResponse` adalah Model hasil penerjemah dari JSON oleh RoboPOJOGenerator yang digunakan untuk menampung data yang diambil dari API sementara.  
    - `CoronaService.java` 
        ```java
        public class CoronaService {
            // inisialisasi Retrofit
            private Retrofit retrofit = null;
        
            // Untuk method getAPI() ini digunakan untuk menhubungkan aplikasi kita dengan url kita tadi dan sekaligus mengubah menjadi jenis GSON.
            public CoronaAPI getAPI(){
                if (retrofit == null){
                    retrofit = new Retrofit
                            .Builder()
                            .baseUrl(CoronaAPI.URL_BASE)
                            .addConverterFactory(GsonConverterFactory.create())
                            .build();
                }
                return retrofit.create(CoronaAPI.class);
            }
        
            // Untuk method getCountries untuk mengambil data dari url kita tadi dengan menggunakan method getCorona(). Setelah itu method onResponse() akan dibaca apabila data sukses diambil dan mengembalikan datanya. Sedangkan method onFailure() akan dibaca apabila data gagal diambil dan mengembalikan nilai errornya.
            public void getCountries(final CoronaListener<List<CountriesItem>> listener){
                getAPI().getCorona().enqueue(new Callback<CoronaResponse>() {
                    @Override
                    public void onResponse(Call<CoronaResponse> call, Response<CoronaResponse> response) {
                    // response.body() adalah hasil data yang diperoleh dari API lalu disimpan ke dalam variabel data
                    CoronaResponse data = response.body();
        
                        if (data != null && data.getGlobal() != null ){
                            listener.onSuccess(data.getCountries());
                        }
                }
        
                    @Override
                    public void onFailure(Call<CoronaResponse> call, Throwable t) {
                        listener.onFailed(t.getMessage());
                    }
                });
            }
        }
        ```
        Kodingan diatas akan ada error pada CoronaListener karena kita belum membuat interface CoronaListener.
        
9. Selanjutnya kita akan membuat 1 interface lagi di dalam package utama dengan nama `CoronaListener` **(Bukan di dalam package model maupun service)** dan sesuaikan isinya menjadi seperti ini.

    ```java
    public interface CoronaListener<T> {
        void onSuccess(T items);
        void onFailed(String msg);
    }
    ```
    Interface ini berfungsi untuk menghubungkan antara `MainActivity.java` dengan `CoronaService.java`.

10. Jika kita lihat kembali fungsi dari interface _**CoronaListener.java**_, selanjutnya kita akan masuk ke dalam `MainActivity.java` dan sesuaikan kodingannya menjadi seperti berikut ini,
    ```java
    public class MainActivity extends AppCompatActivity {
        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_main);
    
            new CoronaService().getCountries(CountryListener);
        }
    
        CoronaListener<List<CountriesItem>> CountryListener = new CoronaListener<List<CountriesItem>>() {
            @Override
            public void onSuccess(List<CountriesItem> items) {
                for(int i = 0; i < items.size(); i++){
                    Log.d("Hasil : NAMA KOTA -> ", items.get(i).getCountry());
                    Log.d("Hasil : CONFIRMED -> ", String.valueOf(items.get(i).getTotalConfirmed()));
                }
            }
    
            @Override
            public void onFailed(String msg) {
                Log.d("ISI ERROR", msg);
            }
        };
    }
    ```
    Kodingan diatas merupakan kodingan untuk mengubungkan antara `MainActivity.java` dengan method `getCountries()` pada kelas `CoronaService.java`. Ketika sukses maka method `onSuccess()` akan dibaca dan ketika gagal method `onFailed()` akan dibaca.


**Note :**
- `getCountry()` adalah salah satu method yang ada di dalam kelas `CountriesItem.java`, anda dapat mengeluarkan data lain dengan melihat nama method yang ada di kelas tersebut.
- Anda dapat mengembangkan kembali project ini dengan mengintegrasikan project ini dengan materi sebelumnya seperti RecyclerView.
- Untuk melihat apakah project berjalan dengan sempurna adalah dengan melihat pada bagian bawah klik _**Logcat**_ -> Ubah _**Verbose**_ menjadi _**Debug**_ dan cari dengan keyword `NAMA KOTA :` seperti dibawah ini.

    ![Logcat](https://gitlab.com/args06/pelatihan-android-itc-2020/-/raw/master/images/logcat.png)
    
    Untuk memahami lebih lanjut silakan buka tautan ini :
    [Retrofit— A simple Android tutorial](https://medium.com/@prakash_pun/retrofit-a-simple-android-tutorial-48437e4e5a23)
    [Consuming APIs with Retrofit](https://guides.codepath.com/android/consuming-apis-with-retrofit)