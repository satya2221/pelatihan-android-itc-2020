# Instalasi
Sebelum mempelajari pemrograman android, tentunya langkah awal yang perlu dilakukan adalah melakukan instalasi JDK dan android studio. untuk penjelasan cara penginstalan, kalian dapat mengikuti langkah dibawah ini.

## Mendownload Android Studio & JDK
### Android Studio : 

Langkah Untuk Windows : 
1. Download android studio versi terbaru di https://developer.android.com/studio?hl=id.
2. Jika sudah mendownload file .exe (direkomendasikan), klik dua kali untuk meluncurkannya.
3. Ikuti wizard penyiapan di Android Studio dan instal paket SDK apa pun yang direkomendasikan.
4. Untuk lebih jelas, bisa dilihat langkahnya pada video di https://developer.android.com/studio/videos/studio-install-windows.mp4?hl=id.
5. Selesai.

Langkah Untuk Linux : 
1. Ekstrak file .zip yang telah Anda download ke lokasi yang tepat untuk aplikasi Anda, misalnya dalam /usr/local/ untuk profil pengguna Anda, atau /opt/ untuk pengguna bersama. Jika menggunakan Linux versi 64 bit, pastikan Anda menginstal library wajib untuk komputer 64 bit terlebih dahulu.

    > sudo apt-get install libc6:i386 libncurses5:i386 libstdc++6:i386 lib32z1 libbz2-1.0:i386

    Jika menjalankan Fedora 64 bit, perintahnya adalah:
    > sudo yum install zlib.i686 ncurses-libs.i686 bzip2-libs.i686

2. Untuk meluncurkan Android Studio, buka satu terminal, arahkan ke direktori android-studio/bin/, lalu jalankan studio.sh.
3. Pilih apakah Anda ingin mengimpor setelan Android Studio sebelumnya atau tidak, lalu klik OK.
4. Wizard Penyiapan Android Studio akan memandu Anda menyelesaikan penyiapan, termasuk mendownload komponen Android SDK yang diperlukan untuk pengembangan.
5. Untuk lebih jelas, bisa dilihat langkahnya pada video di https://developer.android.com/studio/videos/studio-install-linux.mp4?hl=id.
6. Selesai.

> Note : *Pastikan selalu mempunyai koneksi internet ketika menggunakan Android Studio

### JDK
JDK(Java Development Kit) dapat di install dengan cara berikut.

Langkah : 
1. Download JDK sesuai dengan versi yang diinginkan di https://www.oracle.com/java/technologies/javase-downloads.html.
2. Klik 2 kali pada file yang sudah di download, kemudian ikuti perintah instalasi.
3. Setting path environment variable pada control panel > system > advanced > environment variables, lalu tulisakan path dari JDK.
    >Contoh : C:\Program Files\Java\jdk-11\bin
4. Selesai.

# Running Aplikasi

## Menggunakan Virtual Device Sebagai Emulator Android
Android Emulator menyimulasikan perangkat Android di komputer, sehingga kita dapat menguji aplikasi di berbagai perangkat dan API level Android tanpa harus memiliki setiap perangkat fisik.

Dengan emulator kita dapat melakukan simulasi panggilan telepon masuk dan pesan teks, menetapkan lokasi perangkat, melakukan simulasi kecepatan jaringan yang berbeda, melakukan simulasi rotasi dan sensor hardware lainnya, mengakses Google Play Store, dan banyak lagi.

Langkah untuk menginstall emulator : 
1. Jalankan android studio.
2. Klik configure > AVD manager
3. Klik Create Virtual Device.
4. Sesuaikan hardware sesuai kebutuhan lalu klik next.
5. Pilih level API yang diinginkan lalu klik nect.
6. Ubah nama sesuai keinginan.
7. Finish.

## Menggunakan HP Android Sebagai Emulator 
Selain menggunakan virtual device, android studio juga mendukung penggunakan hp sebagai emulator android. Penggunaan hp lebih disarankan karena pada penggunaan virtual device, komputer akan bekerja lebih banyak yang dapat menyebabkan lag.

Langkah untuk menggunakan hp : 
1. Buka hp android, lalu buka setting > about phone.
2. Klik build number berkali kali sampai muncul tulisan developer option.
3. Kembali ke setting lalu buka developer options.
4. Scroll kebawah sampai terdapat pilihan usb debugging.
5. Nyalakan usb debugging.
6. Sambungkan HP dengan komputer menggunakan kabel.
7. Setelah tersambung akan ada pemberitahuan usb debugging.
8. Konfirmasi usb debugging.
9. Lalu buka android studio.
10. Ubah device sesuai hp yang digunakan pada dropdown disamping tombol run.
10. Jika berhasil maka aplikasi akan diinstal dan terbuka di hp android.

untuk lebih jelasnya bisa lihat video tutorial berikut https://www.youtube.com/watch?v=DBisCeEg8Xo 

# Basic Java
## Pendahuluan
Java adalah bahasa pemrograman yang dibuat oleh James Gosling. Java dirancang untuk tujuan umum (general-purpose) dan sepenuhnya menganut paradigma OOP (Object Oriented Programming).

**Kegunaan Java :**
1. Mobile applications (specially Android apps)
2. Desktop applications
3. Web applications
4. Web servers and application servers
5. Games
6. Database connection

**Keunggulan Java :**
1. Java Dapat Dijalankan di Platform yang Berbeda(Windows, Mac, Rasberry Pi, DLL).
2. Open-Source dan Gratis.
3. Comumunity Support yang Luas(Lebih dari 10 Juta Devoloper).
4. Java Merupakan Bahasa Program yang Berbasis Object.
5. Syntax yang Mirip dengan C++ atau C#.

## Struktur dan Syntax pada Java
Setiap bahasa pemrograman memiliki struktur dan aturan penulisan sintaks yang berbeda-beda. Coba perhatikan program berikut:
```java
package com.ITC;
class Program {
    public static void main(String args[]){
        System.out.println("Hello World");
    }
}
```
Dari contoh diatas dapat kita lihat bahwa berbeda dengan C++, pada java banyak struktur struktur yang sedikit asing terutama untuk para developer yang menggunakan bahasa pemrograman seperti C++, Python, dll.

Struktur program Java secara umum dibagi menjadi 4 bagian:
1. **Deklarasi Package**
Package merupakan sebuah folder yang berisi sekumpulan program Java. Tujuan dari penggunaan package adalah agar file dapat tersusun dengan rapih terlebih pada saat membuat program atau aplikasi besar.
2. **Impor Library**
Library merupakan sekumpulan class dan fungsi yang bisa kita gunakan dalam membuat program. Jika kita memerlukan fungsi dari class lain maka kita dapat mengimpor library ke dalam file java kita.
3. **Bagian Class**
Java merupakan bahasa pemrograman yang menggunakan paradigma OOP (Object Oriented Programming). Setiap program harus dibungkus di dalam class agar nanti bisa dibuat menjadi objek. Nama Class harus sama seperti nama file java.
4. **Method Main**
Method main() atau fungsi main() merupakan blok program yang akan dieksekusi pertama kali sama seperti int main() pada C++. Method main merupakan entri point dari program.
Contoh Penulisan : 
    ```java
    package com.ITC; // Deklarasi package
    import java.io.File; // Impor library
    class Program { // Class
        public static void main(String args[]){ // Method
            System.out.println("Hello World");
        }
    }
    ```

## Java OOP

OOP adalah teknik pemrograman yang **berorientesikan Objek**. Dalam OOP **method(Fungsi)** dan **attribute(Variable)** dibungkus oleh objek atau class. Dengan memanfaatkan OOP, kode yang ditulis menjadi semakin baik dan rapih karena sudah terstruktur oleh class. Dalam OOP terdapat konsep dasar yang perlu dipahami seperti Inheritance, Encapsulation, Polimofisme, dan Interface.

### Inheritance
Inheritance atau pewarisan adalah class baru yang “mewarisi” atau memiliki bagian-bagian dari class yang sudah ada sebelumnya. Fungsi dari inheritance kita Tidak harus menyalin semua data dan method dari suatu kelas jika akan menggunakannya lagi, maka kode akan menjadi lebih singkat dan efektif.

Contoh : 
```java
public class Salam { 	
	public void pagi() {
		System.out.println("Halo Selamat Pagi");
	}
}

public class Perkenalan extends Salam { 	
	public void nama() {
		System.out.println("Nama saya ada");
	}

    public static void main(String [] args){
        Perkenalan perkenalan = new Perkenalan();
        perkenalan.pagi();
        perkenalan.nama();
    }
}
```

### Encapsulation (Setter dan Getter)
Setter dan Getter pada java kita gunakan apabila kita ingin mengases suatu variable private atau protected(Jika bukan inhretance) dari suatu class.

Contoh : 
```java
private String data;
public String getData(){//Fungsi Getter
    return this.data;
}

public void setData(String data) {//Fungsi Setter
    this.data = data;
}
```
> Note : 
> Pastikan modifier fungsi getter dan setter adalah public agar fungsi dapat diakses oleh class lain.
> Tipe data variable dan fungsi getter harus sama dan pada setter fungsi bertipe void.

### OverLoading & Overriding

OverLoading & Overriding disebut juga dengan sebutan **Polimorfisme**. Polimorfisme dalam OOP adalah sebuah prinsip di mana class dapat memiliki banyak bentuk method yang berbeda-beda meskipun namanya sama. Berbeda bentuk artinya memiliki isi berbeda, parameter berbeda, atau tipe datanya berbeda.

#### OverLoading

OverLoading terjadi apabila ada 2 method atau lebih yang memiliki nama sama, tetapi memiliki parameter yang berbeda. Tujuan penggunaan overloading yaitu memungkinkan programmer untuk membuat method dengan nama yang sama dengan parameter berbeda. Sehingga dapat membuat program lebih jelas dan mudah dibaca.


Syarat OverLoading:
- Method memiliki nama yang sama harus memiliki paramater berbeda.

Contoh : 
```java
public class Contoh {
    public void hasil(int a, int b) {
        System.out.println("Pertambahan 2 angka = "+ (a+b));
    }
    public void hasil(int a, int b, int c) {
        System.out.println("Pertambahan 3 angka = "+ (a+b+c));
    }
    public void hasil(float a, int b) {
        System.out.println("Pertambahan angka tipe data float dengan integer "+ (a+b));
    }
    public void hasil(int a, float b) {
        System.out.println("Pertambahan angka tipe data interger dengan float "+ (a+b));
    }

}
```

#### Overriding

Overriding berarti menindih atau lengkapnya adalah sebuah method yang terdapat pada subclass dan memiliki nama method yang sama seperti method pada superclassnya. Terdapat keuntungan ketika meng-override method, yaitu super class selaku pemilik overriden method, kodenya tidak perlu mengalami perubahan sama sekali. Sementara itu di sisi sub class, kita dapat mengimplementasikan kode tersebut sesuai dengan kebutuhan.

> Note : 
> Overriding merupakan salah satu manfaat dari penggunaan inheritance.

Syarat Overriding : 
- Nama method dan parameter method pada subclass sama dengan nama method pada superclass.
- Hak akses pada method overriding tidak boleh lebih ketat dari  method pada superclass.

Contoh : 
```java
class supperClass {
    int pertambahan() {
        System.out.println("Hasil Pertambahan dari a + b adalah ");
        return 0;
    }
    int pengurangan() {
        System.out.println("Hasil Pengurangan dari a + b adalah ");
        return 0;
    }
}
 //class subClass mewarisi class supperClass
 class subClass extends supperClass {
    int a;
    int b;
    int pertambahan() {
        return a + b;
    }
}
```

### Interface

Interface atau antarmuka adalah template untuk class dimana semua method di dalamnya harus diimplementasikan ulang oleh class yang memakainya. Secara sederhana, Object Interface adalah sebuah ‘kontrak’ atau perjanjian implementasi method. Bagi class yang menggunakan object interface, class tersebut harus mengimplementasikan ulang seluruh method yang ada di dalam interface. Keuntungan penggunaan interface adalah menutupi kekurangan pada java yang hanya memperbolehkan satu kelas saja yang berhak mendapatkan warisan kelas induk (extends). Sehingga satu kelas hanya dapat menggunakan satu kelas induk, sebaliknya pada interface dapat di implementasi **lebih dari satu**.

Contoh : 

```java
interface Buku {
        public void cover();
        public void judul();
        public void Bab();
}

public class BukuSatu implements Buku{

    @Override
    public void cover() {
        System.out.println("Covernya adalah ini");
    }

    @Override
    public void judul() {
        System.out.println("Judul adalah itu");
    }

    @Override
    public void Bab() {
        System.out.println("Bab ada yes");
    }

    public static void main(String [] args){
        BukuSatu bukusatu = new BukuSatu();
        bukusatu.cover();
        bukusatu.judul();
        bukusatu.Bab();
    }
}
```

link referensi lain untuk belajar :
- [Java Basic](https://www.youtube.com/playlist?list=PLZS-MHyEIRo51w0Hmqi0C8h2KWNzDfo6F)
- [OOP Java](https://www.youtube.com/playlist?list=PLZS-MHyEIRo6V4_vk1s1NcM2HoW5KFG7i)
